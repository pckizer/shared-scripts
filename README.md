Available scripts:

* rsync-backups-via-stunnel

        Use stunnel(8) to securely send quick backups from one host to another
        Requires shared key configured on both ends
        Secure connection requires stunnel forwarding and receiving set up near the end-points, can be forwarded

