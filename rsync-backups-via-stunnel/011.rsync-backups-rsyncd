#!/bin/sh

if [ -r /etc/defaults/periodic.conf ]; then
        . /etc/defaults/periodic.conf
        source_periodic_confs
fi

rc=0

echo
echo "======================"
echo "=== Backups: Rsync ==="
echo "======================"
echo

RSYNCD_USER=${daily_backup_rsyncd_user:-user}
RSYNCD_SERVER=${daily_backup_rsyncd_server:-127.0.0.1}
RSYNCD_PORT=${daily_backup_rsyncd_port:-875}
RSYNCD_TARGET=${daily_backup_rsyncd_target:-backup-target}
RSYNCD_PASSWORD_FILE=${daily_backup_rsyncd_passfile:-/invalid/.rsyncd-password}
RSYNCD_CONFIG_DIR=${daily_backup_rsyncd_confir:-/usr/local/etc/rsync}
RSYNCD_FS_TYPES=${daily_backup_rsyncd_filesystem_types:-zfs,ufs}
RSYNCD_RECURSE=${daily_backup_rsyncd_recurse:-}
RSYNCD_IGNORE=${daily_backup_rsyncd_ignore:-}
RSYNCD_ALLOW=${daily_backup_rsyncd_allow:-}

# Disable further filename expansion to end of script
set -f

case "$daily_backup_rsyncd_enable" in
    [Yy][Ee][Ss])

	for mount_point in `df -t "${RSYNCD_FS_TYPES}" | awk '{print$NF}'` ; do
		# Skip anything that is not a filesystem path
		expr "X${mount_point}" : "^X/.*" > /dev/null || continue

		echo
		echo "Beginning processing of: ${mount_point}"
		echo "======================================="

		RECURSION=""

		for fs_recurse in ${RSYNCD_RECURSE} ; do
	#                  echo "Testing [${mount_point}] for recursion against: ${fs_recurse}"
			if expr "X${mount_point}" : "^X${fs_recurse}$" > /dev/null ; then
				echo "Marking for RECURSION: [${mount_point}]"
				RECURSION=-r
				if [ "${mount_point}" = / ] ; then
					RSYNCD_IGNORE="${RSYNCD_IGNORE} ${fs_recurse}.."'*'
				else
					RSYNCD_IGNORE="${RSYNCD_IGNORE} ${fs_recurse}/.."'*'
				fi
	#                          echo "IGNORE list is now: [${RSYNCD_IGNORE}]"
			fi
		done

		for fs_ignore in ${RSYNCD_IGNORE} ; do
	#                  echo "Testing [${mount_point}] for skipping against: ${fs_ignore}"
			if expr "X${mount_point}" : "^X${fs_ignore}$" > /dev/null ; then
				echo "Skipping: ${mount_point}"
				continue 2
			fi
		done

                if [ -n "${RSYNCD_ALLOW}" ] ; then
                        for fs_allow in ${RSYNCD_ALLOW} ; do
                                if expr "X${mount_point}" : "^X${fs_allow}$" > /dev/null ; then
                                        : Allowed
                                else
                                        echo "Skipping mount not listed in allow list: ${mount_point}"
                                        continue 2
                                fi
                        done
                fi

		echo time /usr/bin/lockf -t 600 sh /usr/local/sbin/do-rsync-backup-rsyncd ${RECURSION} \
		      -u "${RSYNCD_USER}" \
		      -s "${RSYNCD_SERVER}" \
		      -p "${RSYNCD_PORT}" \
		      -t "${RSYNCD_TARGET}" \
		      -P "${RSYNCD_PASSWORD_FILE}" \
		      -c "${RSYNCD_CONFIG_DIR}" \
		      "${mount_point}"

		time /usr/bin/lockf -t 600 sh /usr/local/sbin/do-rsync-backup-rsyncd ${RECURSION} \
		      -u "${RSYNCD_USER}" \
		      -s "${RSYNCD_SERVER}" \
		      -p "${RSYNCD_PORT}" \
		      -t "${RSYNCD_TARGET}" \
		      -P "${RSYNCD_PASSWORD_FILE}" \
		      -c "${RSYNCD_CONFIG_DIR}" \
		      "${mount_point}"

		if [ $? -eq 0 ] ; then
			GOOD="$GOOD ${mount_point}"
		else
			BAD="$BAD ${mount_point}"
		fi

        done
        ;;

    *) echo "Skipping: Daily backups not enabled" ;;
esac

echo ""
echo "==== Backup Summary ===="
echo
echo "Good: ${GOOD}"
echo
echo "BAD:  ${BAD}"
echo

exit 0

