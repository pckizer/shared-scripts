Usage:

Rsyncd Backups
==============

The default is to automatically backup any filesystems of the specified types (default ZFS and UFS) without recursing out of the filesystem.

If a filesystem would be included that needs to be skipped, add it to the space-separated list:

        daily_backup_rsyncd_ignore="/list/of /filesystems /to/be/skipped"

If recursion is needed for a filesystem it can be chosen by setting:

        daily_backup_rsyncd_target="/list/of /filesystems /to/recursively /backup /top-level/and/all/children"

Any filesystems marked for recursion will automatically have all of their children added to the exclusion list.

If only a filesystem or two are desired, they can be listed in an allow list and anything not matching will be skipped:

        daily_backup_rsyncd_allow="/list/of /a/few/filesystems /to/be/allowed"

This can be done on the command-line if needing to take an out-of-normal-cycle backup of a single filesystem with:

        daily_backup_rsyncd_allow="/some/specific/filesystem" /path/to/011.rsync-backups-rsyncd



Stunnel Setup
-------------

This is recommended if you are performing remote backups over untrusted networks.

If you have a trusted link or otherwise forced to not use stunnel then just use the normal rsyncd port of 873 and skip to the Server-side setup section:

        daily_backup_rsyncd_port=873

* Confirure client-side stunnel:

        [rsyncs2backuphost]
        accept = 127.0.0.1:875
        connect = backup-host.example.org:874

* Configure remote server-side stunnel:

        [rsyncs]
        accept = 0.0.0.0:874
        connect = localhost-or-1-hop-destination-rsyncd:873
        client = no

Rsyncd Server/Receiver-side Setup
---------------------------------

* Configure rsyncd.cond:

        [backup-clientname]
            path = /top-level/root/for/backups/for/clientname
            max connections = 0
            uid = root
            gid = wheel
            comment = clientname
            read only = false
            write only = false
            hosts allow = 127.0.0.1 [or server-side stunnel endpoint]
            auth users = clientuser
            secrets file = /some/secure/path/.rsync/secrets.txt

* Configure secrets file on receiving end:

        clientname:some-long-complex-password
        otherclient:another-long-complex-password

Rsyncd Client/Source-side Setup
-------------------------------

* Configure secret on the client side:

        % cat /read-only-to-root/.rsync-password
        some-long-complex-password

* Modify the caller script with the filesystems to be backed-up or use your normal OS methods to set variables (like /etc/periodic.conf for FreeBSD):

        % grep backup_rsync /etc/periodic.conf.local
        daily_backup_rsyncd_enable=yes
        daily_backup_rsyncd_user=user
        daily_backup_rsyncd_server=127.0.0.1
        daily_backup_rsyncd_port=875
        daily_backup_rsyncd_target=backup-target
        daily_backup_rsyncd_passfile=/some/secure/path/.rsyncd-password
        daily_backup_rsyncd_confir=/usr/local/etc/rsync
        daily_backup_rsyncd_recurse=""
        daily_backup_rsyncd_ignore="/ignore /usr/obj"

